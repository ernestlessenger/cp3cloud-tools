package com.cp3cloud.sf.obm.pojo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.InputStream;

/**
 * Parses an Outbound Message and returns a list of notifications
 * TODO: Implement this logic as a SAX parser for better performance
 * @see Notification
 */
public class Parser {
    private static DocumentBuilderFactory factory;
    private static DocumentBuilder builder;

    /**
     * Set up some basic security properties
     */
    static {
        try {
            factory = DocumentBuilderFactory.newInstance();
            String FEATURE = null;

            FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
            factory.setFeature(FEATURE, true);

            FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
            factory.setFeature(FEATURE, false);

            factory.setXIncludeAware(false);
            factory.setExpandEntityReferences(false);

            builder = factory.newDocumentBuilder();
        }
        catch (Exception e) {
            builder = null;
        }
    }

    /**
     * Parses a dynamically-generated SOAP notification
     * @param body an InputStream containing the raw XML body of the SOAP Notification
     * @return
     */
    public static Request parse(InputStream body) {
        Request response = new Request();

        try {

            Document doc = builder.parse(body); // Null pointer exception might be caught here

            NodeList elements = doc.getElementsByTagName("Notification");

            NodeList orgIds = doc.getElementsByTagName("OrganizationId");
            Element orgIdNode = (Element)orgIds.item(0);
            String orgId = orgIdNode.getTextContent();
            if (orgId == null)
                throw new Exception("Unable to identify orgId");

            response.orgId = orgId;

            for (int i = 0; i < elements.getLength(); i++) {
                Notification notification = new Notification();
                notification.orgId = orgId;

                Element node = (Element)elements.item(i);

                Element id = (Element)node.getElementsByTagName("Id").item(0);
                notification.id = id.getTextContent();

                Element sObject = (Element)node.getElementsByTagName("sObject").item(0); // Get the only sObject child
                String type = sObject.getAttribute("xsi:type");
                notification.type = (type.split("\\:")[1]);

                NodeList fields = sObject.getChildNodes();

                for (int j = 0; j < fields.getLength(); j++) {
                    if (fields.item(j).getNodeType() != Node.ELEMENT_NODE)
                        continue;

                    Element ele = (Element)fields.item(j);
                    String key = ele.getTagName().split("\\:")[1];
                    String value = ele.getTextContent();

                    if (key.equalsIgnoreCase("Id"))
                        notification.recordId = value;
                    else if (key.equalsIgnoreCase("RecordTypeId"))
                        notification.recordtype = value;

                    notification.fields.put(key, value);
                }

                response.notifications.add(notification);
            }

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            for (StackTraceElement s : e.getStackTrace())
                System.out.println(s.toString());
            return response;
        }

        return response;
    }
}
