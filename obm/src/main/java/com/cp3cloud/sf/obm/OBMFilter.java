package com.cp3cloud.sf.obm;

import com.cp3cloud.sf.obm.pojo.Parser;
import com.cp3cloud.sf.obm.pojo.Request;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * A generic filter for handling Salesforce Outbound Message requests.
 * This filter will parse the obm request and set two attributes:
 * obm: An object of type Request containing the complete list of notifications
 * orgId: The orgId of the Salesforce org sending this request
 * <p>
 * The filter will send Success back to Salesforce if and only if you set the OBMSuccess attribute to Boolean(true).
 * The filter will never send any content that you provide to the com.cp3cloud.sf.web.Response - only a properly formatted success or failure message will be returned to the client.
 *
 * @see Request
 */
public class OBMFilter implements Filter {
    private ServletContext ctx;
    private Set<String> allowedOrgs = null;

    public void init(FilterConfig config) {
        ctx = config.getServletContext();
        ctx.log("OBMFilter started");

        String strAllowedOrgs = config.getInitParameter("allowedOrgs");
        if (strAllowedOrgs == null || strAllowedOrgs.length() == 0)
            allowedOrgs = null;
        else {
            String[] orgs = StringUtils.split(strAllowedOrgs, ',');

            allowedOrgs = new HashSet<String>();
            allowedOrgs.addAll(Arrays.asList(orgs));
        }
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (!(servletRequest instanceof HttpServletRequest)) {
            throw new ServletException("Unexpected ServletRequest: " + servletRequest.getClass().getName());
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Request parsed = null;
        Boolean isSuccess = false;

        try {
            // Parse the request (consumes the InputStream)
            parsed = Parser.parse(request.getInputStream());

            if (allowedOrgs != null && allowedOrgs.size() > 0 && !allowedOrgs.contains(parsed.orgId))
                throw new ServletException("Invalid orgId: " + parsed.orgId);

            servletRequest.setAttribute("obm", parsed);
            servletRequest.setAttribute("orgId", parsed.orgId);

            OBMResponseWrapper wrapper = new OBMResponseWrapper((HttpServletResponse) response);

            filterChain.doFilter(request, wrapper);
            Boolean requestSuccess = (Boolean) request.getAttribute("OBMSuccess");
            if (requestSuccess != null && requestSuccess)
                isSuccess = true;
        } catch (Exception e) {
            ctx.log(e.getMessage(), e);
            isSuccess = false;
        }

        response.setStatus(HttpServletResponse.SC_OK);
        if (isSuccess)
            IOUtils.write(ACK_SUCCESS, response.getOutputStream());
        else
            IOUtils.write(ACK_FAILURE, response.getOutputStream());
    }

    public void destroy() {

    }

    public final static String ACK_SUCCESS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "\t<soapenv:Body>\n" +
            "\t\t<notifications xmlns=\"http://soap.sforce.com/2005/09/outbound\">\n" +
            "\t\t\t<Ack>true</Ack>\n" +
            "\t\t</notifications>\n" +
            "\t</soapenv:Body>\n" +
            "</soapenv:Envelope>";

    public final static String ACK_FAILURE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "\t<soapenv:Body>\n" +
            "\t\t<notifications xmlns=\"http://soap.sforce.com/2005/09/outbound\">\n" +
            "\t\t\t<Ack>false</Ack>\n" +
            "\t\t</notifications>\n" +
            "\t</soapenv:Body>\n" +
            "</soapenv:Envelope>";
}
