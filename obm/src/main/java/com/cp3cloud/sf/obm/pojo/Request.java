package com.cp3cloud.sf.obm.pojo;

import java.util.List;
import java.util.Vector;

/**
 * Created by ernes on 5/21/2017.
 */
public class Request {
    /**
     * The list of individual record notifications
     */
    public List<Notification> notifications;

    /**
     * The Organization Id of the source org for this notification
     */
    public String orgId;

    public Request() {
        notifications = new Vector<Notification>();
    }
}
