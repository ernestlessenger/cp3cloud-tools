package com.cp3cloud.sf.obm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp3cloud.sf.obm.pojo.Notification;
import com.cp3cloud.sf.obm.pojo.Parser;
import com.cp3cloud.sf.obm.pojo.Request;

/**
 * Always returns success to an Outbound Message request
 */
public class Success extends HttpServlet {
    private ServletContext ctx;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Success() {
        super();
    }

    @Override
    public void init(ServletConfig config) {
        ctx = config.getServletContext();
        ctx.log("Success started");
    }

    /**
     * @throws IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("OBMSuccess", true);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Parse the request (consumes the InputStream)
        Request parsed = Parser.parse(request.getInputStream());
        ctx.log("Found orgId: " + parsed.orgId);

        for (Notification n : parsed.notifications) {
            ctx.log("Found " + n.recordId + " of type " + n.type);
        }

        /**
         * This is the only way to indicate success.
         * You should never set OBMSuccess to true until AFTER your transaction is complete
         */
        request.setAttribute("OBMSuccess", true);
    }

}
