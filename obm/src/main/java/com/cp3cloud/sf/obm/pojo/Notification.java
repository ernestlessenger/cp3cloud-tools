package com.cp3cloud.sf.obm.pojo;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kopitubruk.util.json.JSONUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores an individual Outbound Message notification
 */
public class Notification {
	/**
	 * The Organization Id of the source org for this notification
	 */
	public String orgId;

	/**
	 * The ID of the notification itself
	 */
	public String id;

	/**
	 * The Record ID of the record triggering the notification
	 */
	public String recordId;

	/**
	 * The sObject type of the record
	 */
	public String type;

	/**
	 * If available, the Record Type of the object
	 */
	public String recordtype;

	/**
	 * A map of fieldName -> Value
	 */
	public Map<String, String> fields = new HashMap<String, String>();
    
    public String toJson() {
		return JSONUtil.toJSON(this);
    }

	public String toXML() {
		try {
			StringWriter sw = new StringWriter();

			sw.append("<Notification id=\"" + StringEscapeUtils.escapeXml11(recordId) + "\" orgId=\"" + StringEscapeUtils.escapeXml11(orgId) + "\" type=\"" + StringEscapeUtils.escapeXml11(type) + "\">");

			for (String key : fields.keySet()) {
				sw.append("<" + key + ">");
				sw.append(StringEscapeUtils.escapeXml11(fields.get(key)));
				sw.append("</" + key + ">");
			}

			sw.append("</Notification>");

			sw.close();
			return sw.toString();
		}
		catch (Exception e) {
			return e.getMessage();
		}
	}
}
