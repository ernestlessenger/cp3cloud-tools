package com.cp3cloud.sf.obm;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.CharArrayWriter;
import java.io.PrintWriter;

/**
 * A response wrapper for the OBMFilter that ensures no content is ever written back to the calling service
 */
public class OBMResponseWrapper extends HttpServletResponseWrapper {
    private CharArrayWriter output;

    @Override
    public String toString() {
        return output.toString();
    }

    public OBMResponseWrapper(HttpServletResponse response){
        super(response);
        output = new CharArrayWriter();
    }

    @Override
    public PrintWriter getWriter(){
        return new PrintWriter(output);
    }
}
