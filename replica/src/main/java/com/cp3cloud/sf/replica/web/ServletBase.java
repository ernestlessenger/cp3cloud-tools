package com.cp3cloud.sf.replica.web;

import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.Logger;

public abstract class ServletBase extends HttpServlet {
	protected static final String pom = "0.0.14";
	private static final long serialVersionUID = 3121927606937523697L;
	private static final String PROPS_ORG = "ORGID";
	private static final String PROPS_DEBUG = "DEBUG";
	
	/**
	 * An internal cache of HBase indexed fields
	 */
	protected Set<String> indexedFields = new HashSet<String>();

	protected static Boolean DEBUG = false;

    protected Logger logger = null;
	protected String orgId = null;
	
	// HBase Settings
	protected Connection conn;
	protected String DEFAULT_COLUMN_FAMILY = null;
	protected String DEFAULT_NAMESPACE = null;
	
	/**
	 * An internal cache of HBase tables (may be used by multiple threads)
	 */
	private Map<String, HTable> mapTables = new HashMap<String, HTable>();

	@Override
	public void init() throws ServletException {

		this.DEFAULT_COLUMN_FAMILY = System.getenv("HADOOP_COLUMNFAMILY");
		this.DEFAULT_NAMESPACE = System.getenv("HADOOP_NAMESPACE");
		
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
	        BasicConfigurator.configure();
		}

		if (orgId == null) {
			orgId = System.getenv(PROPS_ORG);
			logger.info("Target orgId is: " + orgId);
		}

		DEBUG = Boolean.getBoolean(System.getenv(PROPS_DEBUG));

		// TODO: Use the team's standard HBaseClient interface
		Configuration conf = HBaseConfiguration.create();
		try {
			logger.info("Initializing HBase");
			if (System.getenv("HADOOP_SECURITY_AUTHENTICATION") != null) {
				logger.info("Initializing HBase (Kerberos)");
				conf.set("hadoop.security.authentication", System.getenv("HADOOP_SECURITY_AUTHENTICATION"));
				System.setProperty("java.security.krb5.conf", System.getenv("JAVA_SECURITY_KRB5_CONF"));
				UserGroupInformation.setConfiguration(conf);
				UserGroupInformation.loginUserFromKeytab(System.getenv("HADOOP_SECURITY_SERVICEACCOUNT"),
						System.getenv("HADOOP_SECURITY_KEYTAB"));
			}
			else {
				conf.set("hbase.master", System.getenv("HBASE_MASTER"));
				conf.set("hbase.rootDir", System.getenv("HBASE_ROOTDIR"));
				conf.set("hbase.zookeeper.quorum", System.getenv("HBASE_ZOOKEEPER_QUORUM"));
				conf.set("hbase.zookeeper.property.clientPort", System.getenv("HBASE_ZOOKEEPER_PROPERTY_CLIENTPORT"));
			}
			
			conf.set("hbase.client.retries.number", Integer.toString(1));
            conf.set("zookeeper.session.timeout", Integer.toString(60000));
            conf.set("zookeeper.recovery.retry", Integer.toString(1));
			
			logger.info("Initialized HBase");
				
		} catch (IOException e) {
			throw new ServletException("Unable to connect to Kerberos setup files");
		}

		try {
			conn = ConnectionFactory.createConnection(conf);
			logger.info("Created HBase Connection");
		} catch (Exception e) {
			throw new ServletException(e);
		}

		//Populate the map of indexed fields
		try {
			HTable mdTable = getTable("sf_meta_field");
			Scan scan = new Scan();
			scan.addFamily(Bytes.toBytes("meta"));
			scan.addFamily(Bytes.toBytes("op"));
			scan.setMaxVersions(1);
			
			ResultScanner mdScanner = mdTable.getScanner(scan);
			
			for (Result rr = mdScanner.next(); rr != null; rr = mdScanner.next()) {
				String field = Bytes.toString(rr.getValue(Bytes.toBytes("meta"), Bytes.toBytes("name")));
				String object = Bytes.toString(rr.getValue(Bytes.toBytes("meta"), Bytes.toBytes("object")));
				String isexternalid = Bytes.toString(rr.getValue(Bytes.toBytes("meta"), Bytes.toBytes("isExternalId")));
				String isindexfield = Bytes.toString(rr.getValue(Bytes.toBytes("op"), Bytes.toBytes("isIndexField")));
				
				if (!(isindexfield.equalsIgnoreCase("true") || isexternalid.equalsIgnoreCase("true")) ) continue;
				
				indexedFields.add(object + "." + field);
			}
			
			mdScanner.close();
			mdTable.close();
			logger.info("Found " + indexedFields.size() + " index fields");
		}
		catch (Exception e) {
			throw new ServletException(e);
		}

		/*
		if (ExternalIDMap.isEmpty()) {
			String indexes = System.getenv(PROPS_INDEXES);
			
			logger.info("Indexed fields are: " + indexes);
			
			if (indexes != null && indexes.length() > 1) {
				for (String idx : indexes.split(",")) {
					String[] arrIdx = idx.split("\\.");
					String table = arrIdx[0].toLowerCase().trim();
					String field = arrIdx[1].toLowerCase().trim();

					if (!ExternalIDMap.containsKey(table))
						ExternalIDMap.put(table, new HashSet<String>());
					ExternalIDMap.get(table).add(field);
				}
			}
		}
		*/
	}
	
	private static final long SIXTY_MINUTES = 3600000L;
	private long lastLogin = 0L;
	protected synchronized void krefresh() throws IOException {
		Long now = System.currentTimeMillis();
		if (lastLogin < now - SIXTY_MINUTES)
			UserGroupInformation.getLoginUser().checkTGTAndReloginFromKeytab();
	}
	
	/**
	 * Returns the cached HTable object for this table, creating one if it does not already exist
	 * @param tblName
	 * @return
	 * @throws IOException
	 */
	protected synchronized HTable getTable(String tblName) throws IOException {
		HTable tbl = mapTables.get(tblName);
		if (tbl == null) {
			tbl = (HTable)conn.getTable(TableName.valueOf(DEFAULT_NAMESPACE, tblName));
			mapTables.put(tblName, tbl);
		}
		return tbl;
	}

}
