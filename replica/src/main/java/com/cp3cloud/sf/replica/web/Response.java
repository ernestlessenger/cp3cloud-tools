package com.cp3cloud.sf.replica.web;

/**
 * Default Outbound Message response strings
 * @author elessenger
 *
 */
public class Response {
    public final static String ACK_SUCCESS = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "\t<soapenv:Body>\n" +
            "\t\t<notifications xmlns=\"http://soap.sforce.com/2005/09/outbound\">\n" +
            "\t\t\t<Ack>true</Ack>\n" +
            "\t\t</notifications>\n" +
            "\t</soapenv:Body>\n" +
            "</soapenv:Envelope>";

    public final static String ACK_FAILURE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "\t<soapenv:Body>\n" +
            "\t\t<notifications xmlns=\"http://soap.sforce.com/2005/09/outbound\">\n" +
            "\t\t\t<Ack>false</Ack>\n" +
            "\t\t</notifications>\n" +
            "\t</soapenv:Body>\n" +
            "</soapenv:Envelope>";
}
