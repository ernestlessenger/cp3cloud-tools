package com.cp3cloud.sf.replica.web;

import com.cp3cloud.sf.obm.pojo.*;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Parses an Outbound Messsage and puts the fields into a queue (specified in
 * the URL), formatted as JSON
 */
public class Enqueue extends ServletBase {
    private static final Integer MESSAGE_FORMAT = 1; // 1 = XML, 2 = MAP
    private static final String PROPS_AMQ_CONNECTION = "JMS_ACTIVEMQENDPOINT";
    private static final String AMQ_QUEUE_PROXY_DESTINATION = "sfdcETFCProxyMessages";
    private static final String AMQ_QUEUE_CHATTER_DESTINATION = "SfChatterQueue";
    private static final String SF_CHATTER_NAME = "SF_Chatter";
//	private static final String queues = "SF_CaseCreate,SF_CaseUpdate,SF_Chatter,SF_Task,SF_TradeBlotter";
//	private static final String topics = "SF_TESTTopic";

    private static ActiveMQConnectionFactory connectionFactory = null;
    private Connection connection = null;

    private ServletContext ctx;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Enqueue() {
        super();
    }

    @Override
    public void init(ServletConfig config) {
        ctx = config.getServletContext();
        ctx.log("Enqueue started");
    }

    @Override
    public void init() throws ServletException {
        super.init();

        // Create a ConnectionFactory
        if (connectionFactory == null)
            connectionFactory = Class.forName();
//             connectionFactory = new ActiveMQConnectionFactory(System.getenv(PROPS_AMQ_CONNECTION));

        // Create a Connection
        try {
            logger.info("Creating connection to " + connectionFactory.getBrokerURL());
            connection = connectionFactory.createConnection();
            connection.start();
        } catch (JMSException e) {
            throw new ServletException(e);
        }
    }

    @Override
    public void destroy() {
        super.destroy();

        // Close the Connection
        try {
            logger.info("Closing Connection");
            connection.close();
        } catch (JMSException e) {
            logger.error("Unable to close connection: " + e.getMessage());
        }
    }

    private static final long serialVersionUID = 1L;

    /**
     * @throws IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_OK);
        IOUtils.write(Response.ACK_SUCCESS, response.getOutputStream());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int msgCount = 0;
        String strDestination = null;

        Session session = null;
        MessageProducer producer = null;

        try {
            // Parse the request (consumes the InputStream)
            Request parsed = Parser.parse(request.getInputStream());
            logger.debug("Parsed " + parsed.notifications.size() + " notifications");

            // Get the name of the topic to post to
            String pathInfo = request.getPathInfo();
            strDestination = pathInfo.substring(pathInfo.lastIndexOf("/") + 1);
            logger.debug("Requested post to " + strDestination);

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = null;
            if (SF_CHATTER_NAME.equalsIgnoreCase(strDestination)) {
                logger.debug("Posting to queue." + AMQ_QUEUE_CHATTER_DESTINATION);
                destination = session.createQueue(AMQ_QUEUE_CHATTER_DESTINATION);
            } else {
                logger.debug("Posting to queue." + AMQ_QUEUE_PROXY_DESTINATION);
                destination = session.createQueue(AMQ_QUEUE_PROXY_DESTINATION);
            }

            // Create a MessageProducer from the Session to the Topic or Queue
            logger.debug("Creating producer");
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            logger.debug("Processing Notification");
            for (Notification notification : parsed) {
                if ((this.orgId != null) && !(notification.orgId.startsWith(this.orgId)))
                    throw new Exception("Unrecognized orgId " + notification.orgId + " / " + this.orgId);

                Message message = null;
                msgCount++;

                // Create a message
                if (MESSAGE_FORMAT == 1) {
                    String text = null;
                    if (SF_CHATTER_NAME.equalsIgnoreCase(strDestination)) {
                        text = notification.toChatterXML();
                    } else {
                        text = notification.toXML();
                    }
                    message = session.createTextMessage(text);
                } else if (MESSAGE_FORMAT == 2) {
                    message = session.createMapMessage();
                    for (String key : notification.fields.keySet()) {
                        ((MapMessage) message).setString(key, notification.fields.get(key));
                    }
                }

                // Add headers
                message.setStringProperty("id", notification.recordId);
                message.setStringProperty("orgid", notification.orgId);
                message.setStringProperty("type", notification.type);
                message.setJMSType(strDestination);
                message.setJMSCorrelationID(notification.id);
                message.setStringProperty("recordtypeid", notification.recordtype); // Might be null

                // Add a header for each ExternalID field contained in the
                // message
                for (String key : notification.fields.keySet()) {
                    if (indexedFields.contains(notification.type + "." + key)) {
                        message.setStringProperty(key, notification.fields.get(key));
                    }
                }

                // Tell the producer to send the message
                logger.debug("Sending message");
                producer.send(message); // Synchronous send
                logger.debug("Sent message " + message.getJMSMessageID().toString());
            }

            response.setStatus(HttpServletResponse.SC_OK);
            IOUtils.write(Response.ACK_SUCCESS, response.getOutputStream());
            if (DEBUG) {
                IOUtils.write("<!-- \n\n", response.getOutputStream());
                IOUtils.write("Wrote " + msgCount + " messages to " + strDestination + "\n\n",
                        response.getOutputStream());
                IOUtils.write(" -->", response.getOutputStream());
            }
        } catch (Exception e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_OK);
            IOUtils.write(Response.ACK_FAILURE, response.getOutputStream());
            if (DEBUG) {
                IOUtils.write("<!-- \n\n", response.getOutputStream());
                IOUtils.write(e.getMessage() + "\n\n", response.getOutputStream());
                for (StackTraceElement ste : e.getStackTrace())
                    IOUtils.write(ste.toString() + "\n", response.getOutputStream());
                IOUtils.write(" -->", response.getOutputStream());
            }
        } finally {
            logger.debug("Closing Producer and Session");
            try {
                if (producer != null)
                    producer.close();

                if (session != null)
                    session.close();
            } catch (Exception e) {
                logger.error("Unable to close Producer or Session");
            }
        }
    }

}
