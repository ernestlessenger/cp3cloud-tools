package com.cp3cloud.sf.replica.web;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import org.apache.hadoop.conf.Configuration;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.joda.time.DateTime;
import org.joda.time.format.*;

/**
 * Parses an Outbound Messsage and puts the field into an HBase database
 */
@WebServlet(urlPatterns = { "/Archive" })
public class Archive extends ServletBase {

	private static DateTimeFormatter fmt = ISODateTimeFormat.dateTime();

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	public void destroy() {
		super.destroy();

		try {
			conn.close();
		} catch (IOException e) {
			System.err.println("Unable to close connection: " + e.getMessage());
		}
	}

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Archive() {
		super();
	}

	/**
	 * @throws IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		IOUtils.write(Response.ACK_SUCCESS, response.getOutputStream());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		krefresh();
		
		int msgCount = 0;
		try {
			List<Notification> parsed = Parser.parse(request.getInputStream());
			logger.debug("Parsed " + parsed.size() + " notifications");

			// Create the list of rows per table
			// It will be faster to do this than to create the table and send
			// one row at a time
			Map<String, List<Put>> putMap = new HashMap<String, List<Put>>();
			for (Notification notification : parsed) {
				if ((this.orgId != null) && !(notification.orgId.startsWith(this.orgId)))
					throw new Exception("Unrecognized orgId " + notification.orgId + " / " + this.orgId);

				msgCount++;
				String table = notification.type;

				long ts = 0;
				if (notification.fields.containsKey("lastmodifieddate")) {
					try {
						DateTime dt = fmt.parseDateTime(notification.fields.get("lastmodifieddate"));
						ts = dt.getMillis();
					} catch (Exception e) {
						ts = 0;
					}
					logger.debug("Found lastmodifieddate field: " + ts);
				}

				// Instantiating Put class for the row
				Put p = new Put(Bytes.toBytes(notification.recordId));
				for (String key : notification.fields.keySet()) {
					String value = notification.fields.get(key);
					
					if (ts > 0)
						p.addColumn(Bytes.toBytes(DEFAULT_COLUMN_FAMILY), Bytes.toBytes(key), ts, Bytes.toBytes(value));
					else
						p.addColumn(Bytes.toBytes(DEFAULT_COLUMN_FAMILY), Bytes.toBytes(key), Bytes.toBytes(value));

					// Update the index table, if the column is an indexed column
					if (indexedFields.contains(table + "." + key)) {
						Put p2 = new Put(Bytes.toBytes(notification.fields.get(key))); // The "key" field in an index is always the value of the field
						if (ts > 0)
							p2.addColumn(Bytes.toBytes(DEFAULT_COLUMN_FAMILY), Bytes.toBytes("id"), ts,
									Bytes.toBytes(notification.recordId)); // The "Id" field in an index is always the record ID
						else
							p2.addColumn(Bytes.toBytes(DEFAULT_COLUMN_FAMILY), Bytes.toBytes("id"),
								Bytes.toBytes(notification.recordId)); // The "Id" field in an index is always the record ID
						
						addToTable(putMap, "idx_" + table + "_" + key, p2);
					}
				}

				addToTable(putMap, table, p);
			}

			logger.debug("Putting " + putMap.size() + " tables to HBase");

			// Put the rows into the tables, using the default table namespace
			for (String strTable : putMap.keySet()) {
				try {
					logger.debug("Opening " + strTable);
					HTable table = (HTable)conn.getTable(TableName.valueOf(DEFAULT_NAMESPACE, strTable));

					// Put all of the requests for each table in a single
					// operation
					List<Put> puts = putMap.get(strTable);
					
					logger.debug("Putting " + puts.size() + " rows to " + strTable);
					
					table.put(puts);

					logger.debug("Closing " + strTable);
					table.close();
				} catch (Exception e) {
					System.err.println("Failed to write to table " + strTable);
				}
			}

			response.setStatus(HttpServletResponse.SC_OK);
			IOUtils.write(Response.ACK_SUCCESS, response.getOutputStream());
			if (DEBUG) {
				IOUtils.write("<!-- \n\n", response.getOutputStream());
				IOUtils.write("Wrote " + msgCount + " messages\n\n", response.getOutputStream());
				IOUtils.write(" -->", response.getOutputStream());
			}
		} catch (Exception e) {
			logger.error(e);
			response.setStatus(HttpServletResponse.SC_OK);
			IOUtils.write(Response.ACK_FAILURE, response.getOutputStream());
			if (DEBUG) {
				IOUtils.write("<!-- \n\n", response.getOutputStream());
				IOUtils.write(e.getMessage() + "\n\n", response.getOutputStream());
				for (StackTraceElement ste : e.getStackTrace())
					IOUtils.write(ste.toString() + "\n", response.getOutputStream());
				IOUtils.write(" -->", response.getOutputStream());
			}
		}
	}

	/**
	 * Adds a row to the supplied map, in the correct list for the provided
	 * table. This allows us to iterate over the list of tables and make a
	 * single Put call per table at the end of the request
	 * 
	 * @param putMap
	 *            the map storing the lists of rows per table
	 * @param table
	 *            the name of the table to insert the row to
	 * @param put
	 *            the row to insert to the table
	 */
	private void addToTable(Map<String, List<Put>> putMap, String table, Put put) {

		List<Put> puts = putMap.get(table);
		if (puts == null) {
			puts = new Vector<Put>();
			putMap.put(table, puts);
		}
		puts.add(put);
	}
}
