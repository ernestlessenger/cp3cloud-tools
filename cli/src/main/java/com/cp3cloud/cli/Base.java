package com.cp3cloud.cli;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * The base class from which all CumuloLibris Java command line tools inherit
 *
 * @author Ernest W Lessenger
 */
public abstract class Base {

    /**
     * Contains the logic for this Command Line utility
     */
    protected abstract void execute() throws Exception;

    /**
     * @return	Null, or a map of required arguments for this Command Line utility.
     */
    protected Map<String, String> requiredArgs() { return new HashMap<String, String>(); }

    /**
     * @return	Null, or a map of optional arguments for this Command Line utility.
     */
    protected Map<String, String> optionalArgs() {
        return null;
    }

    /**
     * This must be overridden because Java doesn't allow overridden Static methods, and there is no way for this
     * instance of the method to know which implementing Class to instantiate.
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.err.println("This Utility does not support command line access");
    }

    /**
     * The pattern used to validate and match parameter names
     */
    private static final String strParamPattern = "^--([a-zA-Z0-9._{}+-,]+)$";

    /**
     * Empty constructor
     */
    public Base() {
    }

    /**
     * The properties file to be passed to all other objects
     */
    protected Properties myProps = new Properties();

    /**
     * The logger used for this class
     */
    private static final Logger logger = LogManager.getLogger(Base.class);

    /**
     * Creates a new Command Line class from the provided arguments and list of required arguments
     *
     * @param args			An array of command line arguments
     * @throws Exception
     */
    public void execute(String[] args) throws Exception {
        // Validate that there are an even number of properties
        if (args.length % 2 != 0) {
            printUsage("An even number of arguments (--parameter value) is required:");
        }


        // Overwrite the settings file with any properties provided on the
        // command line
        for (Integer i = 0; i < args.length; i += 2) {
            String longParam = args[i];
            if (!longParam.matches(strParamPattern)) {
                printUsage("An even number of arguments (--parameter value) is required.");
            }

            String key = longParam.substring(2);
            String value = args[i + 1];
            myProps.put(key, value);
        }

        Properties tmpProps = new Properties();
        Set<String> includedProperties = new HashSet<String>();// Keep a list of files that have already been included

        // Load the settings file
        if (myProps.containsKey("settings")) {
            File f = new File(myProps.getProperty("settings"));
            tmpProps.load(new FileInputStream(f));
            includedProperties.add(f.getCanonicalPath());

            // The command line properties ALWAYS overwrite loaded properties
            tmpProps.putAll(myProps);
            myProps = tmpProps;

            logger.debug("Loaded settings from: " + f.getCanonicalPath());
        } else {
            printUsage("A settings file is required");
        }

        // Load the included properties
        Boolean hasMore = false;
        Properties loadedProperties = new Properties();
        do {
            hasMore = false;
            for (Object objKey : myProps.keySet()) {
                String key = (String)objKey;
                if (key.startsWith("settings.include.")) {
                    String strFile = myProps.getProperty(key);
                    try {
                        File f = new File(strFile);

                        // We only want to load any given file once
                        // And we stop when we have loaded all files
                        if (includedProperties.contains(f.getCanonicalPath()))
                            break;
                        hasMore = true;
                        includedProperties.add(f.getCanonicalPath());

                        // Load the file itself
                        Properties includeProperties = new Properties();
                        includeProperties.load(new FileInputStream(f));
                        loadedProperties.putAll(includeProperties);

                        logger.debug("Loaded settings from: " + f.getCanonicalPath());
                    }
                    catch (Throwable ex) {
                        printUsage("Unable to load settings file: " + ex.getMessage());
                    }
                }
            }
        } while (hasMore); // We stop when we have not loaded any additional files in this pass

        myProps.putAll(loadedProperties);
        loadedProperties = null;

        // Log completion
        logger.info("Loaded settings from " + myProps.getProperty("settings"));

        // Initialize this object
        execute(myProps);
    }

    /**
     * Initializes the properties attribute by cloning the provided set of Properties;
     * Validates that all required arguments were provided;
     * Sets up the log4j Logger
     *
     * @param props			A Java Properties object
     * @throws Exception
     */
    public void execute(Properties props) throws Exception {
        this.myProps = (Properties)props.clone();

        Map<String, String> requiredArgs = this.requiredArgs();

        // Validate that the required fields are present
        for (Object objKey : this.myProps.keySet()) {
            String key = (String)objKey;
            requiredArgs.remove(key);
        }
        if ( requiredArgs != null && requiredArgs.size() > 0) {
            printUsage("One or more required properties is missing:");
        }

        requiredArgs = null;

        // Only reconfigure the logger once per CLI
        if (!this.myProps.containsKey("com.cumulolibris.cli.configured")) {

            // Record which CLI was the source of this configuration
            this.myProps.put("com.cumulolibris.cli.configured", this.getClass().getName());
            this.logger.info("Configured Logger from " + this.getClass().getName());

            // Set the process id to a random string

            ThreadContext.push(UUID.randomUUID().toString()); // Add the fishtag;
        }

        Date started = new Date();
        this.logger.info(this.getClass().getName() + " started: " + started.toString());

        // Print the required arguments to the debug log
        if (this.requiredArgs() != null) {
            for (String key : this.requiredArgs().keySet()) {
                this.logger.debug(key + "=" + this.myProps.get(key));
            }
        }

        // Execute the calling class's execute method
        this.execute();

        Date completed = new Date();
        Long seconds = (completed.getTime() - started.getTime()) / 1000L;
        this.logger.info(this.getClass().getName() + " completed: " + completed.toString() + " after " + seconds + " seconds");
    }

    /**
     * Prints the command line usage to stdout. If an error message is provided,
     * then the message is written to stdout as well.
     *
     * @param error			An optional error message
     * @throws Exception
     */
    protected final void printUsage(String error) throws Exception {

        Map<String, String> requiredArgs = this.requiredArgs();
        Map<String, String> optionalArgs = this.optionalArgs();

        System.out
                .println("Usage:\t"
                        + this.getClass().getName()
                        + " --settings path-to-settings [--parameter value] [--parameter value] ...");
        System.out.println();

        if (error != null) {
            System.out.println("\tError: " + error);
            System.out.println();
        }

        if (requiredArgs != null) {
            System.out.println("\tRequired Arguments:");
            for (String key : requiredArgs.keySet()) {
                System.out.println("\t\t" + key + " - " + requiredArgs.get(key));
            }
            System.out.println();
        }

        if (optionalArgs != null) {
            System.out.println("\tOptional Arguments:");
            for (String key : optionalArgs.keySet()) {
                System.out.println("\t\t" + key + " - " + optionalArgs.get(key));
            }
            System.out.println();
        }

        System.out.println();

        System.out.println("\tThe settings file will be loaded as a standard java Properties file.");
        System.out.println();
        System.out.println("\tYou can include additional settings files by providing a path to them in a setting named settings.include.* (where * is any text string).");
        System.out.println("\tIf the parameter 'settings.log' is set, then the references file will be loaded and used to configure log4j.");
        System.out.println("\t\tOtherwise, the settings file itself will be used to configure log4j.");
        System.out.println();
        System.out.println("\tYou may specify an arbitrary number of additional parameters...");
        System.out.println("\t\tParameters set in this manner will overwrite the parameters loaded from file.");
        System.out.flush();

        if (error != null)
            throw new Exception(error);
    }

    /**
     * @param key
     * @return A string representation of the indicated property
     */
    public String getProperty(String key) {
        return this.myProps.getProperty(key);
    }

    /**
     * @param key
     * @param value
     * @return This object (useful for chaining calls to setProperty)
     */
    public Base setProperty(String key, String value) {
        this.myProps.setProperty(key, value);
        return this;
    }

    /**
     * @param key
     * @return An Integer representation of the indicated property
     */
    public Integer getIntegerProperty(String key) {
        String strValue= this.getProperty(key);
        try {
            return Integer.parseInt(strValue);
        }
        catch (NumberFormatException ex) {
            logger.error("Unable to parse parameter " + key + " with value " + strValue + " as Integer");
            return null;
        }
    }

    /**
     * @param key
     * @return A Long representation of the indicated property
     */
    public Long getLongProperty(String key) {
        String strValue= this.getProperty(key);
        try {
            return Long.parseLong(strValue);
        }
        catch (NumberFormatException ex) {
            logger.error("Unable to parse parameter " + key + " with value " + strValue + " as Long");
            return null;
        }
    }

    /**
     * @param key
     * @return A Double representation of the indicated property
     */
    public Double getDoubleProperty(String key) {
        String strValue= this.getProperty(key);
        try {
            return Double.parseDouble(strValue);
        }
        catch (NumberFormatException ex) {
            logger.error("Unable to parse parameter " + key + " with value " + strValue + " as Double");
            return null;
        }
    }

    /**
     * @param key
     * @return A Date representation of the indicated property
     */
    @SuppressWarnings("deprecation")
    public Long getDateProperty(String key) {
        String strValue= this.getProperty(key);
        try {
            return Date.parse(strValue);
        }
        catch (NumberFormatException ex) {
            logger.error("Unable to parse parameter " + key + " with value " + strValue + " as Date");
            return null;
        }
    }
}

