package com.cp3cloud.sf.ant;

import org.apache.tools.ant.Task;

/**
 * A base class for Salesforce Ant tasks.
 * Supported properties:
 *   username - the Salesforce org username
 *   password - the Salesforce password and security code (if required)
 *   serverUrl - the Salesforce production or test login url
 *   sessionId - the Salesforce session Id; set after a successful connection
 *     to Salesforce.
 *   
 * @author elessenger
 * @see Apache Task documentation: http://javadoc.haefelinger.it/org.apache.ant/1.7.1/org/apache/tools/ant/Task.html
 * @see Tutorial: Writing Tasks: http://ant.apache.org/manual/tutorial-writing-tasks.html
 */
public class TaskBase extends Task {
  private String username;
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  
  private String password;
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  
  private String serverurl;
  public String getServerurl() {
    return serverurl;
  }
  public void setServerurl(String serverurl) {
    this.serverurl = serverurl;
  }
  
  private String sessionId;
  public String getSessionId() {
      return (sessionId == null || sessionId.equals("")) ? null : sessionId;
  }
  public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
  }

  private String instanceUrl;
  public String getInstanceUrl() {
    return (instanceUrl == null || instanceUrl.equals("")) ? null : instanceUrl;
  }
  public void setInstanceUrl(String instanceUrl) {
    this.instanceUrl = instanceUrl;
  }

  private String accessToken;
  public String getAccessToken() {
    return (accessToken == null || accessToken.equals("")) ? null : accessToken;
  }
  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  @Override
  public void execute() {
  }
}
