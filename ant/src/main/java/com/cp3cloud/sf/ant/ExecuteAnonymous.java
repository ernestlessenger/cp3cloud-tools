package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.ApexConnection;
import com.sforce.soap.apex.ExecuteAnonymousResult;
import com.sforce.soap.apex.SoapConnection;
import com.sforce.ws.ConnectionException;
import org.apache.tools.ant.BuildException;

//import javax.xml.rpc.ServiceException;

/**
 * A Salesforce Ant task for executing anonymous Apex. A connection to Salesforce
 * will be established if it doesn't already exist.
 * 
 * Supported properties:
 *   command - the apex code to execute
 */
public class ExecuteAnonymous extends TaskBase {
    private String command;

    public void addText(String command) {
        this.command = command;
    }
    public void setCommand(String command) { this.command = command; }
    public String getCommand() { return this.command; }

    @Override
    public void execute() throws BuildException {
        ApexConnection connection = null;
        if (this.getSessionId() == null)
            connection = new ApexConnection(this.getUsername(),
                    this.getPassword(), this.getServerurl());
        else
            connection = new ApexConnection(this.getSessionId(),
                    this.getServerurl());

        try {
            connection.connect();
        } catch (Exception e) {
            throw new BuildException(e);
        }

        SoapConnection conn = connection.getApexconnection();

        ExecuteAnonymousResult result = null;
        try {
            log(command);
            result = conn.executeAnonymous(command);
        } catch (ConnectionException e) {
            throw new BuildException(e);
        }

        if (result == null) {
            throw new BuildException("Unable to obtain result");
        }
        else if (!result.isSuccess()) {
            throw new BuildException(result.getExceptionMessage());
        }
    }
}
