package com.cp3cloud.sf.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * A Salesforce Ant task for building a package file for all metadata, including
 * folders, for an org. A connection to Salesforce will be established if it
 * doesn't already exist. More information on the Salesforce MetadataConnection
 * object can be found here:
 *   https://developer.salesforce.com/page/Introduction_to_the_Force.com_Web_Services_Connector
 *
 * Supported properties:
 *   dstPath - output file(s) will be stored in this location
 *   hasPortal - not used
 *   folders - if true, folder metadata will be included; default is false
 */
public class MergePackage extends Task {
    private String filename;
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }

    private Vector<FileSet> filesets = new Vector<FileSet>();

    public void addFileset(FileSet fileset) {
        filesets.add(fileset);
    }

    private com.sforce.soap.metadata.MetadataConnection metadataConnection;

    @Override
    public void execute() throws BuildException {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = null;
            Node docRoot = null;

            HashMap<String, Node> nodeMap = new HashMap<String, Node>();

            for (Iterator itFSets = filesets.iterator(); itFSets.hasNext(); ) {
                FileSet fs = (FileSet) itFSets.next();
                DirectoryScanner ds = fs.getDirectoryScanner();
                String[] includedFiles = ds.getIncludedFiles();
                for (int i = 0; i < includedFiles.length; i++) {
                    File base = ds.getBasedir();                               // 5
                    File found = new File(base, includedFiles[i]);

                    log("Reading from: " + found.getCanonicalPath());

                    if (doc == null) {
                        doc = dBuilder.parse(found);
                        docRoot = doc.getDocumentElement();
                    } else {
                        Document tmpDoc = dBuilder.parse(found);
                        Node tmpDocRoot = tmpDoc.getDocumentElement();

                        // TODO: Merge nodes that contain the same <name>value</name> as a child node
                        NodeList lst = tmpDocRoot.getChildNodes();
                        for (int n = 0; n < lst.getLength(); n++) {
                            Node oldNode = lst.item(n);

                            String typeName = getTypeName(oldNode);

                            if (nodeMap.containsKey(typeName)) {
                                Node newNode = nodeMap.get(typeName);

                                NodeList members = oldNode.getChildNodes();
                                for (int m = 0; m < members.getLength(); m++) {
                                    Node oldMember = members.item(m);
                                    if (oldMember.getNodeName().equalsIgnoreCase("members")) {
                                        Node newMember = doc.importNode(oldMember, true);
                                        newNode.appendChild(newMember);
                                    }
                                }

                            }
                            else {
                                Node newNode = doc.importNode(oldNode, true);
                                docRoot.appendChild(newNode);
                                nodeMap.put(typeName, newNode);
                            }
                        }
                    }
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);
            File dst = new File(this.getFilename());
            (new File(dst.getParent())).mkdirs();

            log("Writing to: " + dst.getCanonicalPath());

            StreamResult streamResult = new StreamResult(dst);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, streamResult);
        }
        catch (Exception e) {
            throw new BuildException(e);
        }
    }

    private String getTypeName(Node node) {
        NodeList lst = node.getChildNodes();
        for (int n = 0; n < lst.getLength(); n++) {
            Node childNode = lst.item(n);
            if (childNode.getNodeName().equalsIgnoreCase("name")) {
                return childNode.getTextContent();
            }
        }
        return null;
    }
}
