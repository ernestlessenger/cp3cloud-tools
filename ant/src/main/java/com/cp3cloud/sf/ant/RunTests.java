package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.ApexConnection;
import com.sforce.soap.apex.*;
import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * A Salesforce Ant task for running unit tests. Tests that match the specified
 * namespace will be run and any failures logged to a JUnit result file.
 * <p/>
 * Supported properties:
 * throwOnFailure - task will fail if any tests fail; default is false
 * failOnCoverage - insufficient unit test coverage will be treated as a test failure;
 * default is false
 * namespace - if specified, only tests within this namespace will be tested
 * dstPath - output file(s) will be stored here
 * minCoverage - the acceptable unit test coverage for an Apex file; default is
 * 75%. If the coverage is less than this and "failOnCoverage" is true, the
 * task will fail.
 */
public class RunTests extends TaskBase {
    private Boolean throwOnFailure = false;
    public String getThrowOnFailure() {
        return throwOnFailure.toString();
    }
    public void setThrowOnFailure(String throwOnFailure) {
        this.throwOnFailure = Boolean.valueOf(throwOnFailure);
    }

    private Boolean failOnCoverage = false;
    public String getFailOnCoverage() {
        return failOnCoverage.toString();
    }
    public void setFailOnCoverage(String failOnCoverage) {
        this.failOnCoverage = Boolean.valueOf(failOnCoverage);
    }

    private String namespace;
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    public String getNamespace() {
        return this.namespace;
    }

    private String dstPath;
    public String getDstPath() {
        return dstPath;
    }
    public void setDstPath(String dstPath) {
        this.dstPath = dstPath;
    }

    private Double minCoverage = 75.0;
    public String getMinCoverage() {
        return String.valueOf(minCoverage);
    }
    public void setMinCoverage(String minCoverage) {
        this.minCoverage = Double.valueOf(minCoverage);
    }

    private DocumentBuilderFactory docFactory;
    private DocumentBuilder docBuilder;

    @Override
    public void execute() throws BuildException {
        log("Connecting to " + this.getServerurl());

        ApexConnection connection = null;
        if (this.getSessionId() == null) {
            connection = new ApexConnection(this.getUsername(), this.getPassword(), this.getServerurl());
        } else {
            log("Using provided session ID");
            connection = new ApexConnection(this.getSessionId(), this.getServerurl());
        }

        if (this.failOnCoverage)
            log("Insufficient Unit Test coverge will be treated as a test failure");
        if (this.throwOnFailure)
            log("Task will fail if any tests fail");

        RunTestsResult results = null;
        try {
            log("Establishing a connection");
            // connection.setTracelogs("./tracelogs.txt");
            connection.connect();

            RunTestsRequest request = new RunTestsRequest();
            request.setAllTests(true);

            log("Running tests");
            results = connection.getApexconnection().runTests(request);
        } catch (Exception e) {
            throw new BuildException(e);
        }
        log("Tests complete");

        CodeCoverageResult[] coverages = results.getCodeCoverage();
        RunTestSuccess[] successes = results.getSuccesses();
        RunTestFailure[] failures = results.getFailures();
        HashMap<String, RunTestSuccess> successMap = new HashMap<String, RunTestSuccess>();
        HashMap<String, RunTestFailure> failureMap = new HashMap<String, RunTestFailure>();

        log("Aggregating successes and failures");

        // Get a map of the namespaces, classes, and methods so that we can
        // aggregate them later
        HashMap<String, HashMap<String, HashSet<String>>> nsMap = new HashMap<String, HashMap<String, HashSet<String>>>();
        for (RunTestSuccess success : successes) {
            String apexNamespace = (success.getNamespace() == null) ? "c"
                    : success.getNamespace();
            String apexClass = success.getName();
            String apexMethod = success.getMethodName();
            String fqn = apexNamespace + "__" + apexClass + "." + apexMethod;

            if (!nsMap.containsKey(apexNamespace))
                nsMap.put(apexNamespace, new HashMap<String, HashSet<String>>());
            HashMap<String, HashSet<String>> classMap = nsMap
                    .get(apexNamespace);

            if (!classMap.containsKey(apexClass))
                classMap.put(apexClass, new HashSet<String>());
            Set<String> methodSet = classMap.get(apexClass);

            successMap.put(fqn, success);
            methodSet.add(apexMethod);
        }

        for (RunTestFailure failure : failures) {
            String apexNamespace = (failure.getNamespace() == null) ? "c"
                    : failure.getNamespace();
            String apexClass = failure.getName();
            String apexMethod = failure.getMethodName();
            String fqn = apexNamespace + "__" + apexClass + "." + apexMethod;

            if (!nsMap.containsKey(apexNamespace))
                nsMap.put(apexNamespace, new HashMap<String, HashSet<String>>());
            HashMap<String, HashSet<String>> classMap = nsMap
                    .get(apexNamespace);

            if (!classMap.containsKey(apexClass))
                classMap.put(apexClass, new HashSet<String>());
            Set<String> methodSet = classMap.get(apexClass);

            failureMap.put(fqn, failure);
            methodSet.add(apexMethod);
            // log("  Failure: " + failure.getStackTrace() + "\n" + failure.getMessage());
        }

        log("Generating JUnit XML document");
        if (this.namespace != null)
            log("Outputting only tests for namespace \"" + this.namespace + "\"");

        Document doc = null;
        try {
            if (docFactory == null) docFactory = DocumentBuilderFactory.newInstance();
            if (docBuilder == null) docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
        } catch (ParserConfigurationException e) {
            throw new BuildException(e);
        }

        // root elements
        Element testsuites = doc.createElement("testsuites");

        // Create the unit test results
        for (String apexNamespace : nsMap.keySet()) {

            // We ignore namespaces that were not requested
            if (this.namespace != null && !(this.namespace.equalsIgnoreCase(apexNamespace))) {
                // log("Ommitting namespace \"" + apexNamespace + "\"");
                continue;
            }

            HashMap<String, HashSet<String>> classMap = nsMap
                    .get(apexNamespace);

            // We create one testsuite per class
            for (String apexClass : classMap.keySet()) {
                Element testsuite = doc.createElement("testsuite");
                String pqn = apexNamespace + "__" + apexClass;
                HashSet<String> methodSet = classMap.get(apexClass);

                Integer iSuccesses = 0;
                Integer iFailures = 0;
                // We create one test per method
                for (String method : methodSet) {
                    String fqn = pqn + "." + method;
                    RunTestSuccess success = successMap.get(fqn);
                    RunTestFailure failure = failureMap.get(fqn);

                    Element testcase = doc.createElement("testcase");
                    testcase.setAttribute("name", method);

                    // Successes do not have their own error message
                    if (success != null) {
                        iSuccesses++;
                        testcase.setAttribute("time",
                                String.valueOf(ms2s(success.getTime())));

                    }

                    // But failures do
                    if (failure != null) {
                        iFailures++;
                        testcase.setAttribute("time",
                                String.valueOf(ms2s(failure.getTime())));

                        Element eFailure = doc.createElement("failure");

                        eFailure.setAttribute("type", "Unit Test Failure");
                        eFailure.setAttribute("message", failure.getMessage());
                        eFailure.appendChild(doc.createCDATASection(failure.getStackTrace()));

                        testcase.appendChild(eFailure);
                    }

                    testsuite.appendChild(testcase);
                }

                testsuite.setAttribute("name", pqn);
                testsuites.appendChild(testsuite);
            }
        }

        // Append the Test Coverage
        log("Appending test coverage results");

        Element coverageSuite = doc.createElement("testsuite");
        coverageSuite.setAttribute("name", "Code Coverage");
        for (CodeCoverageResult coverage : coverages) {
            Element coverageCase = doc.createElement("testcase");
            String apexNamespace = (coverage.getNamespace() == null) ? "c" : coverage.getNamespace();
            String apexClass = coverage.getName();
            String pqn = apexNamespace + "__" + apexClass;

            // We ignore namespaces that were not requested
            if (this.namespace != null && !(this.namespace.equalsIgnoreCase(apexNamespace))) {
                // log("Ommitting namespace \"" + apexNamespace + "\"");
                continue;
            }

            coverageCase.setAttribute("name", pqn);

            Integer totalLocation = coverage.getNumLocations();
            Integer uncoveredLocations = coverage.getNumLocationsNotCovered();
            Integer coveredLocations = totalLocation - uncoveredLocations;

            Double coverageRatio = (Double.valueOf(coveredLocations) / Double.valueOf(totalLocation)) * 100.0;
            String strCoverageRatio = String.valueOf(coverageRatio.intValue());

            coverageCase.setAttribute("coverage", String.valueOf(strCoverageRatio));

            // We only log coverage failures that are:
            // (a) below the minimum
            // (b) if we have been told to log a failure
            // (c) in the default namespace
            if (coverageRatio < this.minCoverage && this.failOnCoverage && apexNamespace == "c") {
                Element eFailure = doc.createElement("failure");

                eFailure.setAttribute("type", "Insufficient Code Coverage");
                eFailure.setAttribute("message", "Coverage of " + strCoverageRatio + " is below the minimum value of " + this.minCoverage);

                coverageCase.appendChild(eFailure);
            }

            coverageSuite.appendChild(coverageCase);
        }
        testsuites.appendChild(coverageSuite);

        doc.appendChild(testsuites);

        log("Preparing to write JUnit XML Document");

        try {
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            File dst = new File(this.getDstPath());
            (new File(dst.getParent())).mkdirs();
            log("Writing to: " + dst.getCanonicalPath());

            StreamResult streamResult = new StreamResult(dst);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, streamResult);
        } catch (TransformerException e) {
            throw new BuildException(e);
        } catch (IOException e) {
            throw new BuildException(e);
        }

        log("Tests reported " + results.getNumFailures() + " failures");
        if (results.getNumFailures() > 0 && this.throwOnFailure) {
            throw new BuildException("Tests failed with "
                    + results.getNumFailures() + " failures");
        }
    }

    // Converts from milliseconds to seconds
    private static Double ms2s(Double in) {
        if (in == null || in == 0.0d) return 0.0d;
        return in / 1000.0d;
    }
}
