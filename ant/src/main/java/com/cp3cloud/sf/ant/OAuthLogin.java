package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.RESTConnection;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

/**
 * Connects to Salesforce using a PartnerConnection, then sets project settings
 * sessionId, restEndpoint, soapEndpoint, and authEndpoint.
 * <p/>
 * Supported properties:
 * prefix: the settings prefix; "sf." by default
 */
public class OAuthLogin extends TaskBase {
    private String prefix = "sf.";

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    protected String clientId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    protected String clientSecret;

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public void execute() throws BuildException {
        RESTConnection connection = new RESTConnection(this.getUsername(), this.getPassword(), this.getServerurl(), this.getClientId(), this.getClientSecret());

        log("Establishing a connection");
        try {
            connection.doLogin();
        } catch (Exception e) {
            throw new BuildException(e);
        }

        String access_token = connection.getAccessToken();
        String instance_url = connection.getInstanceUrl();

        Project project = this.getProject();

        log("Access Token: " + access_token);
        project.setProperty(this.getPrefix() + "accessToken", access_token);

        log("Instance Url: " + instance_url);
        project.setProperty(this.getPrefix() + "instanceUrl", instance_url);
    }
}
