package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.PartnerConnection;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

/**
 * Connects to Salesforce using a PartnerConnection, then sets project settings
 * sessionId, restEndpoint, soapEndpoint, and authEndpoint.
 * 
 * Supported properties:
 *   prefix: the settings prefix; "sf." by default
 */
public class Login extends TaskBase {
    private String prefix = "sf.";
    public String getPrefix() {
        return prefix;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

  @Override
  public void execute() throws BuildException {
        PartnerConnection connection = new PartnerConnection(this.getUsername(),
                this.getPassword(), this.getServerurl());

        log("Establishing a connection");
        try {
            connection.connect();
        } catch (Exception e) {
            throw new BuildException(e);
        }

        String sessionId = connection.getSessionId();
        String restEndpoint = connection.getConfig().getRestEndpoint();
        String soapEndpoint = connection.getConfig().getServiceEndpoint();
        String authEndpoint = connection.getConfig().getAuthEndpoint();

        Project project = this.getProject();

        log("Session id: " + sessionId);
        project.setProperty(this.getPrefix() + "sessionId", sessionId);

        log("REST Endpoint: " + restEndpoint);
        project.setProperty(this.getPrefix() + "restendpoint", restEndpoint);

        log("SOAP Endpoint: " + soapEndpoint);
        project.setProperty(this.getPrefix() + "soapEndpoint", soapEndpoint);

        log("Auth Endpoint: " + authEndpoint);
        project.setProperty(this.getPrefix() + "authEndpoint", authEndpoint);
    }
}
