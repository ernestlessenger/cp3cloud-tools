package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.PartnerConnection;
import com.sforce.soap.partner.*;
import com.sforce.ws.ConnectionException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tools.ant.BuildException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * A Salesforce Ant task for exporting a data dictionary for an org.
 * A connection to Salesforce will be established if it
 * doesn't already exist. More information on the Salesforce MetadataConnection 
 * object can be found here:
 *   https://developer.salesforce.com/page/Introduction_to_the_Force.com_Web_Services_Connector
 */
public class DataDictionary extends TaskBase {
    private String objects;
    public String getObjects() {
        return objects;
    }
    public void setObjects(String objects) {
        this.objects = objects;
    }

    private String fields;
    public String getFields() {
        return fields;
    }
    public void setFields(String fields) {
        this.fields = fields;
    }

    private Boolean full = Boolean.FALSE;
    public Boolean getFull() {
        return full;
    }
    public void setFull(Boolean full) {
        this.full = full;
    }

    private DocumentBuilderFactory docFactory;
    private DocumentBuilder docBuilder;

    private com.sforce.soap.partner.PartnerConnection partnerConnection;

    @Override
    public void execute() throws BuildException {
        PartnerConnection connection = null;
        if (this.getSessionId() == null)
            connection = new PartnerConnection(
                    this.getUsername(), this.getPassword(), this.getServerurl());
        else {
            connection = new PartnerConnection(
                    this.getSessionId(), this.getServerurl());
        }

        // connection.setTracelogs("out/trace.log");

        log("Establishing a connection");

        try {
            connection.connect();
            partnerConnection = connection.getPartnerConnection();
        }
        catch (Exception e) {
            throw new BuildException(e);
        }

        CSVPrinter csvObjects = null;
        CSVPrinter csvFields = null;


        log("Opening output files");
        try {
            File outObjects = new File(getObjects());
            BufferedWriter writeObjects = new BufferedWriter(new FileWriter(outObjects)); // Files.newBufferedWriter(Paths.get(getDstPath()));
                csvObjects = new CSVPrinter(writeObjects, CSVFormat.DEFAULT
                        .withHeader("Object Name", "Label", "KeyPrefix", "Custom Object","Custom Setting", "Queryable", "Searchable", "Replicatable", "Creatable", "Updateable", "Deleteable"));

            File outFields = new File(getFields());
            BufferedWriter writeFields = new BufferedWriter(new FileWriter(outFields)); // Files.newBufferedWriter(Paths.get(getDstPath()));
            csvFields = new CSVPrinter(writeFields, CSVFormat.DEFAULT
                    .withHeader("Object Name", "Field Name", "Label", "Type", "Help Text", "Scale","Precision", "Length", "Calculated","Formula", "External Id", "Id Lookup", "Reference To", "Default Value", "Default Value Formula", "Restricted Picklist", "Picklist Values"));
        } catch (IOException e) {
            throw new BuildException(e);
        }

        List<String> sObjects = new Vector<String>();
        try {
            log("Getting global describe");
            DescribeGlobalResult global = partnerConnection.describeGlobal();
            DescribeGlobalSObjectResult[] objects = global.getSobjects();

            Arrays.sort(objects, new Comparator<DescribeGlobalSObjectResult>() {
                public int compare(DescribeGlobalSObjectResult o1, DescribeGlobalSObjectResult o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            for (DescribeGlobalSObjectResult result : objects) {
                if (!this.getFull()) {
                    if (
                            !result.getIdEnabled() &&
                                    !result.getCustom() &&
                                    !result.getCustom()
                            )
                        continue;
                }

                sObjects.add(result.getName());
            }
        } catch (ConnectionException e) {
            throw new BuildException(e);
        }

        log("Describing objects (this may take a while)");

        for (String sObjectName : sObjects) {
            try {
                // log("Describing " + sObjectName);
                DescribeSObjectResult object = partnerConnection.describeSObject(sObjectName);

                csvObjects.printRecord(object.getName(), object.getLabel(), object.getKeyPrefix(), object.getCustom(), object.getCustomSetting(), object.getQueryable(), object.getSearchable(), object.getReplicateable(), object.getCreateable(), object.getUpdateable(), object.getDeletable());

                Field[] fields = object.getFields();
                Arrays.sort(fields, new Comparator<Field>() {
                    public int compare(Field o1, Field o2) {
                        return o1.getName().compareTo(o2.getName());
                    }
                });

                for (Field field : fields) {

                    String picklistvalues = null;
                    PicklistEntry[] entries = field.getPicklistValues();
                    if (entries != null && entries.length > 0) {
                        List<String> strPicklistvalues = new Vector<String>();
                        for (PicklistEntry p : entries) {
                            strPicklistvalues.add(p.getValue());
                        }
                        picklistvalues = String.join(";", strPicklistvalues);
                    }

                    csvFields.printRecord(object.getName(), field.getName(), field.getLabel(), field.getType().toString(), field.getInlineHelpText(), field.getScale(), field.getPrecision(), field.getLength(), field.getCalculated(), field.getCalculatedFormula(), field.getExternalId(), field.getIdLookup(), String.join(";",field.getReferenceTo()), field.getDefaultValue(), field.getDefaultValueFormula(), field.isRestrictedPicklist(), String.join(";",picklistvalues));
                }

            } catch (Exception e) {
                throw new BuildException(e);
            }
        }

        log("Closing output files");

        try {
            csvObjects.close();
            csvFields.close();
        } catch (IOException e) {
            throw new BuildException(e);
        }

    }
}
