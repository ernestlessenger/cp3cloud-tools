package com.cp3cloud.sf.ant;

import com.cp3cloud.sf.services.MetadataConnection;
import com.cp3cloud.sf.services.PartnerConnection;
import com.sforce.soap.metadata.DescribeMetadataObject;
import com.sforce.soap.metadata.DescribeMetadataResult;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.ListMetadataQuery;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.apache.tools.ant.BuildException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * A Salesforce Ant task for building a package file for all metadata, including
 * folders, for an org. A connection to Salesforce will be established if it 
 * doesn't already exist. More information on the Salesforce MetadataConnection 
 * object can be found here:
 *   https://developer.salesforce.com/page/Introduction_to_the_Force.com_Web_Services_Connector
 * 
 * Supported properties:
 *   dstPath - output file(s) will be stored in this location
 *   hasPortal - not used
 *   folders - if true, folder metadata will be included; default is false
 */
public class BuildPackage extends TaskBase {
    private String dstPath;

    public String getDstPath() {
        return dstPath;
    }

    public void setDstPath(String dstPath) {
        this.dstPath = dstPath;
    }

    private Boolean hasPortal = false;

    public Boolean getHasPortal() {
        return hasPortal;
    }

    public void setHasPortal(Boolean hasPortal) {
        this.hasPortal = hasPortal;
    }

    private Boolean folders = false;

    public Boolean getFolders() {
        return folders;
    }

    public void setFolders(Boolean folders) {
        this.folders = folders;
    }

    private DocumentBuilderFactory docFactory;
    private DocumentBuilder docBuilder;

    private com.sforce.soap.metadata.MetadataConnection metadataConnection;

    private Integer packageSize = 100;
    public Integer getPackageSize() {
        return packageSize;
    }
    public void setPackageSize(Integer packageSize) {
        this.packageSize = packageSize;
    }

    @Override
    public void execute() throws BuildException {
        MetadataConnection connection = null;
        if (this.getSessionId() == null)
            connection = new MetadataConnection(
                    this.getUsername(), this.getPassword(), this.getServerurl());
        else {
            connection = new MetadataConnection(
                    this.getSessionId(), this.getServerurl());
        }

        // connection.setTracelogs("out/trace.log");

        log("Establishing a connection");
        try {
            connection.connect();

            metadataConnection = connection.getMetadataConnection();

            if (this.folders)
                this.buildFolderList();

        } catch (Exception e) {
            throw new BuildException(e);
        }

        try {
            if (docFactory == null)
                docFactory = DocumentBuilderFactory.newInstance();
            if (docBuilder == null)
                docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new BuildException(e);
        }

        // Get the list of Metadata types, and output them each as a separate XML file
        try {
            log("Getting list of metadata types...");
            DescribeMetadataResult result = metadataConnection.describeMetadata(MetadataConnection.apiVersion);
            log(" Done.");


            log("Getting metadata types");
            for (DescribeMetadataObject obj : result.getMetadataObjects()) {
                buildDataPackage(obj.getXmlName(), false, true);
            }
        } catch (ConnectionException e) {
            throw new BuildException(e);
        }

    }

    public void buildDataPackage(String prefix, Boolean wildcard, Boolean query) {

        try {
            List<String> types = getTypeList(prefix, wildcard, query);
            Collections.sort(types);
            Integer i = types.size();

            List<List<String>> partitions = new Vector<List<String>>();
            Integer start = 0;
            while (start < i) {
                Integer end = Math.min(start + packageSize, i);
                partitions.add(types.subList(start, end));
                start = end;
            }

            Integer suffix = 1;
            for (List<String> subTypes : partitions) {
                Document doc = docBuilder.newDocument();

                Element pkg = doc.createElement("Package");
                pkg.setAttribute("xmlns", "http://soap.sforce.com/2006/04/metadata");
                doc.appendChild(pkg);

                // Data Package
                pkg.appendChild(getTypes(prefix, doc, subTypes));

                this.writePackage(prefix + "-" + (suffix++) + ".xml", doc);
            }
        } catch (Exception e) {
            throw new BuildException(e);
        }
    }

    /**
     * Writes a fully formed Package.xml file
     *
     * @param filename
     */
    private void writePackage(String filename, Document doc) {
        // log("Preparing to write " + filename);

        try {
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory
                    .newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);
            File dst = new File(this.getDstPath(), filename);
            (new File(dst.getParent())).mkdirs();

            log("Writing to: " + dst.getCanonicalPath());

            StreamResult streamResult = new StreamResult(dst);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, streamResult);
        } catch (TransformerException e) {
            throw new BuildException(e);
        } catch (IOException e) {
            throw new BuildException(e);
        }
    }

    /**
     * Generates the list of metadata elements from a pre-queried list
     *
     * @param componentType The type of metadata component
     * @param members       The list of members
     * @return an XML Element containing the list of metadata elements
     * @throws ConnectionException
     */
    public Element getTypes(String componentType, Document doc, List<String> members) {
        Element typ = doc.createElement("types");

        for (String strMember : members) {
            Element eMember = doc.createElement("members");
            eMember.setTextContent(strMember);
            typ.appendChild(eMember);
        }

        Element name = doc.createElement("name");
        name.setTextContent(componentType);
        typ.appendChild(name);

        return typ;
    }

    /**
     * Generates the list of metadata elements, optionally querying Salesforce for the complete list of metadata components
     *
     * @param componentType The type of metadata component
     * @param wildcard      If true, adds the * component to attempt to download "all" components
     * @param query         If true, queries Salesforce for the complete list of components
     * @return A list of metadata elements
     * @throws ConnectionException
     */
    public List<String> getTypeList(String componentType, Boolean wildcard, Boolean query) throws ConnectionException {
        List ret = new Vector<String>();

        if (query) {
            if (this.foldersByType.containsKey(componentType)) {
                Set<String> folderSet = this.foldersByType.get(componentType);
                for (String folder : folderSet) {

                    List<FileProperties> fileList = this.listMetadata( componentType, folder);
                    for (FileProperties file : fileList) {
                        ret.add(file.getFullName());
                    }
                }
            } else {
                List<FileProperties> list = this.listMetadata(componentType, null);
                for (FileProperties s : list) {
                    ret.add(s.getFullName());
                }
            }
        }

        if (wildcard) {
            ret.add("*");
        }

        return ret;
    }

    public Map<String, HashSet<String>> foldersByType = new HashMap<String, HashSet<String>>();

    /**
     * Queries the Salesforce partner API for the list of items in folders
     *
     * @throws ConnectionException
     */
    private void buildFolderList() throws ConnectionException {
        log("Establishing a Partner connection... ");

        PartnerConnection conn = null;
        if (this.getSessionId() == null)
        conn = new PartnerConnection(this.getUsername(),
                this.getPassword(), this.getServerurl());
        else
            conn = new PartnerConnection(this.getSessionId(), this.getServerurl());

        try {
            conn.connect();
        } catch (Exception e) {
            throw new BuildException(e);
        }

        com.sforce.soap.partner.PartnerConnection partnerConnection = conn
                .getPartnerConnection();

        log("Connected.");
        log("Querying folders... ");

        String queryString = "SELECT Id, DeveloperName, Type FROM Folder ORDER BY Type ASC, DeveloperName ASC";
        partnerConnection.setStreamingEnabledHeader(false);
        QueryResult qResult = partnerConnection.query(queryString);

        // log(qResult.getDone());

        log("Found " + qResult.getSize());

        foldersByType = new HashMap<String, HashSet<String>>();

        if (qResult.getSize() > 0) {
            Boolean done = false;
            while (!done) {
                SObject[] records = qResult.getRecords();
                for (int i = 0; i < records.length; i++) {
                    SObject obj = records[i];

                    String typ = (String) obj.getField("Type");
                    if (typ.equalsIgnoreCase("Email"))
                        typ = "EmailTemplate"; // SPECIAL LOGIC TO HANDLE AN INCONSISTENCY IN SALESFORCE'S APIs

                    String folderName = (String) obj.getField("DeveloperName");

                    if (!foldersByType.containsKey(typ)) {
                        HashSet<String> tmp = new HashSet<String>();
                        tmp.add("unfiled$public");// We have to explicitly add
                        // this because it doesn't
                        // come down in the query
                        // list
                        foldersByType.put(typ, tmp);

                    }

                    foldersByType.get(typ).add(folderName);
                }

                if (qResult.isDone())
                    done = true;
                else
                    qResult = partnerConnection.queryMore(qResult
                            .getQueryLocator());
            }
        }
    }

    /**
     * Gets the list of metadata components
     *
     * @param componentType The type of component to list
     * @param folder        OPTIONAL The folder to download
     * @return A list of metadata names (optionally, for a given folder) of the specified componentType
     * @throws ConnectionException
     */
    public List<FileProperties> listMetadata(String componentType, String folder)
            throws ConnectionException {
        ListMetadataQuery query = new ListMetadataQuery();

        if (folder != null) {
      /*
      * if (componentType.equalsIgnoreCase("emailtemplate")) {
      * componentType = "EmailFolder"; } else { componentType =
      * componentType + "Folder"; }
      */
            query.setType(componentType);
            query.setFolder(folder);
        } else {
            query.setType(componentType);
        }

        List<FileProperties> list = new Vector<FileProperties>();
        try {
            FileProperties[] lmr = metadataConnection.listMetadata(
                    new ListMetadataQuery[]{query}, MetadataConnection.apiVersion);

            if (lmr != null) {
                for (FileProperties n : lmr) {
                    list.add(n);
                }
            }
        }
        catch (Exception e) {
            log("Failed to download Metadata list for " + componentType);
        }

        return list;
    }
}
