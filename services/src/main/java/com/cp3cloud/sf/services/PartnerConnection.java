package com.cp3cloud.sf.services;

import com.sforce.soap.partner.*;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import java.io.FileNotFoundException;

public class PartnerConnection extends Connection {
	private com.sforce.soap.partner.PartnerConnection partnerConnection;
	public com.sforce.soap.partner.PartnerConnection getPartnerConnection() {
		return partnerConnection;
	}

    public PartnerConnection(String username, String password, String serverurl) {
        super(username, password, serverurl);
    }

    public PartnerConnection(String sessionId, String serverurl) {
        super(sessionId, serverurl);
    }

	@Override
	public Boolean connect() throws FileNotFoundException, ConnectionException {
		ConnectorConfig config = this.doLogin();
		if (this.tracelogs != null)
			config.setTraceFile(this.tracelogs);
			
		partnerConnection = Connector.newConnection(config);
		
		partnerConnection.setSessionHeader(config.getSessionId());
		//config.setServiceEndpoint(config.getServiceEndpoint().replaceAll("\\/u\\/", "/m/"));

		return true;
	}

}
