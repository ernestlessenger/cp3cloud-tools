package com.cp3cloud.sf.services;

// import com.sforce.soap.tooling.*;
import com.sforce.ws.ConnectionException;

import java.io.FileNotFoundException;

/**
 * Temporarily deprecated
 */
@Deprecated
public class ToolingConnection extends Connection {
	/*
	private com.sforce.soap.tooling.ToolingConnection toolingConnection;
	public com.sforce.soap.tooling.ToolingConnection getToolingConnection() {
		return toolingConnection;
	}

    public ToolingConnection(String username, String password, String serverurl) {
        super(username, password, serverurl);
    }

    public ToolingConnection(String sessionId, String serverurl) {
        super(sessionId, serverurl);
    }

	@Override
	public Boolean connect() throws FileNotFoundException, ConnectionException {
		ConnectorConfig config = this.doLogin();
		if (this.tracelogs != null)
			config.setTraceFile(this.tracelogs);

		config.setServiceEndpoint(config.getServiceEndpoint().replaceAll("\\/u\\/", "/T/"));

		toolingConnection = Connector.newConnection(config);
		toolingConnection.setSessionHeader(config.getSessionId());

		return true;
	}
	*/


	public ToolingConnection(String username, String password, String serverurl) {
		super(username, password, serverurl);
	}

	@Override
	public Boolean connect() throws FileNotFoundException, ConnectionException {
		return null;
	}
}
