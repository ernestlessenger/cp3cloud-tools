package com.cp3cloud.sf.services;

import com.sforce.ws.ConnectionException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 * Provides the basic abstract settings and operations for connecting to a Salesforce API
 * @author Ernest W. Lessenger
 */
public class RESTConnection {
    public static final double apiVersion = 46.0;
	private static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private static final DateFormat datetimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private static final int READ_TIMEOUT = 30000;
	private static boolean DEBUG = false;

    public RESTConnection(String username, String password, String serverurl, String client_id, String client_secret) {
        this.setUsername(username);
        this.setPassword(password);
		this.setServerurl(serverurl);
		this.setClientId(client_id);
		this.setClientSecret(client_secret);
    }
    public RESTConnection(String instanceUrl, String access_token) {
        this.setAccessToken(access_token);
        this.setInstanceUrl(instanceUrl);
    }
	
	protected String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

    protected String password;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

	protected String serverurl;
	public String getServerurl() {
		return serverurl;
	}
	public void setServerurl(String serverurl) {
		this.serverurl = serverurl;
	}

	protected String clientId;
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	protected String clientSecret;
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	protected String tracelogs;
	public String getTracelogs() {
		return tracelogs;
	}
	public void setTracelogs(String tracelogs) {
		this.tracelogs = tracelogs;
	}

    private String accessToken;
    public String getAccessToken() { return accessToken; }
    public void setAccessToken(String accessToken) { this.accessToken = accessToken; }

    private String instanceUrl;
    public String getInstanceUrl() { return instanceUrl; }
    public void setInstanceUrl(String instanceUrl) { this.instanceUrl = instanceUrl; }

	public boolean doLogin() throws ConnectionException, IOException, ParseException {
		URL url = new URL(this.getServerurl());
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("charset", "utf-8");
		connection.setReadTimeout(READ_TIMEOUT);

		// Generate the request

		Vector<String> connectionStrings = new Vector<String>();
		connectionStrings.add("grant_type=password");
		connectionStrings.add("client_id=" + URLEncoder.encode(this.getClientId(), "UTF-8"));
		connectionStrings.add("client_secret=" + URLEncoder.encode(this.getClientSecret(), "UTF-8"));
		connectionStrings.add("username=" + URLEncoder.encode(this.getUsername(), "UTF-8"));
		connectionStrings.add("password=" + URLEncoder.encode(this.getPassword(), "UTF-8"));

		String strBody = StringUtils.join(connectionStrings, "&");
		byte[] postData = strBody.getBytes();
		connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.write(postData);

		// Connect
		connection.connect();

		// Connect and read the (hopefully) JSON result
		String json = IOUtils.toString(connection.getInputStream());
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(json);

        String access_token = (String)obj.get("access_token");
        this.setAccessToken(access_token);

        String instance_url = (String)obj.get("instance_url");
        this.setInstanceUrl(instance_url);

		return true;
	}

    /**
     * Returns the results of a SOQL query in an already parsed JSON object
     * @param query
     * @return
     * @throws IOException
     * @throws ParseException
     */
	public JSONObject query(String query) throws IOException, ParseException {
        String api_version = String.format("v%3.1f",this.apiVersion);

        URL url = new URL(this.getInstanceUrl() + "/services/data/" + api_version + "/query/?q=" + URLEncoder.encode(query,"UTF-8"));

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Authorization", "Bearer " + this.getAccessToken());
		connection.setReadTimeout(READ_TIMEOUT);

        // Connect
        connection.connect();

        // Connect and read the (hopefully) JSON result
        String json = IOUtils.toString(connection.getInputStream());
        JSONParser parser = new JSONParser();

        JSONObject obj = (JSONObject) parser.parse(json);

        return obj;
    }

	/**
	 * Counts the total number of records matched by the query
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Long countQuery(String query) throws IOException, ParseException {
		JSONObject obj = this.query(query);

		try {
			Long totalSize = (Long)obj.get("totalSize");
			if (totalSize == null) {
				throw new Exception("Null value for TotalSize");
			}
			return totalSize;
		}
		catch (Exception e) {
			System.err.println("Unable to parse totalSize");
			if (DEBUG && obj != null)
				System.err.println(obj.toJSONString());
		}
		return null;
	}

	/**
	 * Counts the total number of records matched by the query
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Calendar calendarFieldQuery(String query, String field) throws IOException, ParseException {
		JSONObject obj = this.query(query);

        String fieldValue = null;
		try {
            JSONArray records = (JSONArray)obj.get("records");
            JSONObject record = (JSONObject)records.get(0);
            fieldValue = (String)record.get(field);

			Date date = datetimeFormatter.parse(fieldValue);
			Calendar c = Calendar.getInstance();
			c.setTime(date);

			return c;
		}
		catch (Exception e) {
			System.err.println("Unable to parse date: " + fieldValue + " for field: " + field);
			if (DEBUG && obj != null)
				System.err.println(obj.toJSONString());
		}

		return null;
	}

    /**
     * Counts the total number of records in the "obj" object
     * @param obj
     * @return
     * @throws IOException
     * @throws ParseException
     */
	public Long countRecords(String obj) throws IOException, ParseException {
		String query = "SELECT Id FROM " + obj;
		return this.countQuery(query);
	}

	public Long countRecords(String obj, String dateField, Integer month, Integer year) throws IOException, ParseException {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1,0,0,0);

		String start = dateFormatter.format(cal.getTime());
		cal.add(Calendar.MONTH, 1);
		String end = dateFormatter.format(cal.getTime());

		String query = "SELECT Id FROM " + obj + " WHERE (" + dateField + " >= " + start + " AND " + dateField + " < " + end + ")";
		return this.countQuery(query);
	}

    /**
     * Counts the total number of records in the "obj" object where "field" is not null
     * @param obj
     * @param field
     * @return
     * @throws IOException
     * @throws ParseException
     */
	public Long countRecords(String obj, String field) throws IOException, ParseException {
		String query = "SELECT Id FROM " + obj + " WHERE (" + field + " != null)";
		return this.countQuery(query);
	}

	public Long countRecords(String obj, String field, String dateField, Integer month, Integer year) throws IOException, ParseException {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1, 0,0,0);

		String start = dateFormatter.format(cal);
		cal.add(Calendar.MONTH, 1);
		String end = dateFormatter.format(cal);

		String query = "SELECT Id FROM " + obj + " WHERE (" + field + " != null) AND (" + dateField + " >= " + start + " AND " + dateField + " < " + end + ")";
		return this.countQuery(query);
	}

	public Calendar getLastModified(String obj) throws IOException, ParseException {
		String query = "SELECT Id, LastModifiedDate FROM " + obj + " ORDER BY LastModifiedDate DESC LIMIT 1";
		return this.calendarFieldQuery(query, "LastModifiedDate");
	}

	public Calendar getFirstModified(String obj) throws IOException, ParseException {
		String query = "SELECT Id, LastModifiedDate FROM " + obj + " ORDER BY LastModifiedDate ASC LIMIT 1";
		return this.calendarFieldQuery(query, "LastModifiedDate");
	}

	public Calendar getLastCreated(String obj) throws IOException, ParseException {
		String query = "SELECT Id, CreatedDate FROM " + obj + " ORDER BY CreatedDate DESC LIMIT 1";
		return this.calendarFieldQuery(query, "CreatedDate");
	}

	public Calendar getFirstCreated(String obj) throws IOException, ParseException {
		String query = "SELECT Id, CreatedDate FROM " + obj + " ORDER BY CreatedDate ASC LIMIT 1";
		return this.calendarFieldQuery(query, "CreatedDate");
	}

	public Calendar getLastModified(String obj, String field) throws IOException, ParseException {
		String query = "SELECT Id, LastModifiedDate FROM " + obj + " WHERE (" + field + " != null) ORDER BY LastModifiedDate DESC LIMIT 1";
		return this.calendarFieldQuery(query, "LastModifiedDate");
	}

	public Calendar getFirstModified(String obj, String field) throws IOException, ParseException {
		String query = "SELECT Id, LastModifiedDate FROM " + obj + " WHERE (" + field + " != null) ORDER BY LastModifiedDate ASC LIMIT 1";
		return this.calendarFieldQuery(query, "LastModifiedDate");
	}

	public Calendar getLastCreated(String obj, String field) throws IOException, ParseException {
		String query = "SELECT Id, CreatedDate FROM " + obj + " WHERE (" + field + " != null) ORDER BY CreatedDate DESC LIMIT 1";
		return this.calendarFieldQuery(query, "CreatedDate");
	}

	public Calendar getFirstCreated(String obj, String field) throws IOException, ParseException {
		String query = "SELECT Id, CreatedDate FROM " + obj + " WHERE (" + field + " != null) ORDER BY CreatedDate ASC LIMIT 1";
		return this.calendarFieldQuery(query, "CreatedDate");
	}
}
