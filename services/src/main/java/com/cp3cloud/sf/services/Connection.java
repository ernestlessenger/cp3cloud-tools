package com.cp3cloud.sf.services;

import com.sforce.soap.partner.LoginResult;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import com.sforce.soap.partner.PartnerConnection;

import java.io.FileNotFoundException;

/**
 * Provides the basic abstract settings and operations for connecting to a Salesforce API
 * @author Ernest W. Lessenger
 */
public abstract class Connection {
	public static final double apiVersion = 46.0;

	public Connection(String username, String password, String serverurl) {
        this.setUsername(username);
        this.setPassword(password);
        this.setServerurl(serverurl);
    }
    public Connection(String sessionId, String serverurl) {
        this.setSessionId(sessionId);
        this.setServerurl(serverurl);
    }
	
	protected String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

    protected String password;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

	protected String serverurl;
	public String getServerurl() {
		return serverurl;
	}
	public void setServerurl(String serverurl) {
		this.serverurl = serverurl;
	}
	
	protected String tracelogs;
	public String getTracelogs() {
		return tracelogs;
	}
	public void setTracelogs(String tracelogs) {
		this.tracelogs = tracelogs;
	}
	
	public abstract Boolean connect() throws FileNotFoundException, ConnectionException;
	
	public Boolean connect(Boolean except) throws FileNotFoundException, ConnectionException {
		Boolean success = false;
		if (except) {
			success = connect();
		}
		else {
			try {
				success = connect();
			}
			catch (Exception e) {
				throw new ConnectionException(e.getMessage());
			}
		}
		return success;
	}
	
	private ConnectorConfig config;
	public ConnectorConfig getConfig() {
		return config;
	}

	private String sessionId;
    public String getSessionId() { return sessionId; }
    public void setSessionId(String sessionId) { this.sessionId = sessionId; }

	public ConnectorConfig doLogin() throws ConnectionException, FileNotFoundException {
		if (this.config != null) return this.config;
		
		this.config = new ConnectorConfig();

        config.setManualLogin(false);
        // config.setCompression(false);
        // config.setConnectionTimeout(60);
        // config.setValidateSchema(false);

        if (tracelogs != null) {
            config.setTraceFile(tracelogs);
            config.setTraceMessage(true);
        }

        config.setPrettyPrintXml(false);

        if (this.getSessionId() == null) {
            config.setUsername(this.getUsername());
            config.setPassword(this.getPassword());
            config.setAuthEndpoint(this.getServerurl());

			com.sforce.soap.partner.PartnerConnection partnerConnection = new PartnerConnection(config);

            LoginResult plr = partnerConnection.login(this.getUsername(), this.getPassword());
            this.setSessionId(plr.getSessionId());

        }
        else {
            config.setSessionId(this.getSessionId());
            config.setServiceEndpoint(this.getServerurl());
        }

		return config;
	}
	
	
}
