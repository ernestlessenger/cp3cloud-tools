package com.cp3cloud.sf.services;

import com.sforce.soap.apex.Connector;
import com.sforce.soap.apex.SoapConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import java.io.FileNotFoundException;

public class ApexConnection extends Connection {
	private SoapConnection apexConnection;

	public SoapConnection getApexconnection() {
		return apexConnection;
	}

	public ApexConnection(String username, String password, String serverurl) {
		super(username, password, serverurl);
	}

    public ApexConnection(String sessionId, String serverurl) {
        super(sessionId, serverurl);
    }

	@Override
	public Boolean connect() throws FileNotFoundException, ConnectionException {
		ConnectorConfig config = this.doLogin();
		if (this.tracelogs != null)
			config.setTraceFile(this.tracelogs);
			
		apexConnection = Connector.newConnection(config);
		apexConnection.setSessionHeader(config.getSessionId());

		config.setServiceEndpoint(config.getServiceEndpoint().replaceAll("\\/u\\/", "/s/"));

		return true;
	}
}
