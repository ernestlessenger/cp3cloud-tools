package com.cp3cloud.sf.services;

import com.sforce.soap.metadata.*;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import java.io.FileNotFoundException;

public class MetadataConnection extends Connection {
	private com.sforce.soap.metadata.MetadataConnection metadataConnection;
	public com.sforce.soap.metadata.MetadataConnection getMetadataConnection() {
		return metadataConnection;
	}

	public MetadataConnection(String username, String password, String serverurl) {
		super(username, password, serverurl);
	}

    public MetadataConnection(String sessionId, String serverurl) {
        super(sessionId, serverurl);
    }

	@Override
	public Boolean connect() throws FileNotFoundException, ConnectionException {

		ConnectorConfig config = this.doLogin();
		if (this.tracelogs != null)
			config.setTraceFile(this.tracelogs);
			
		metadataConnection = Connector.newConnection(config);
		metadataConnection.setSessionHeader(config.getSessionId());

		config.setServiceEndpoint(config.getServiceEndpoint().replaceAll("\\/u\\/", "/m/"));

		return true;
	}

}
